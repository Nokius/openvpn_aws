variable "pvt_key" {}
variable "pub_key" {}

# set the region
variable "region" {
	default = "us-east-1"
}

provider "aws" {
	region = "${var.region}"
}

# set the port where openvpn will listen on
variable "openvpn_port" {
	description = "The port where openvpn will listen on"
	default = "443" # avoiding vpn blocking in hotspots
}

# set the proto which openvpn will use
variable "openvpn_proto" {
	description = "The proto which openvpn will use"
	default = "udp" 
}

# ssh key
resource "aws_key_pair" "provisioner-key" {
	key_name = "provisioner-key"
	public_key = "${file(var.pub_key)}"
}

# define the instance t2.micro should do it
resource "aws_instance" "vpn_openvpn" {
	ami = "ami-43a15f3e"
	instance_type = "t2.micro"
	vpc_security_group_ids = ["${aws_security_group.allow_external.id}"]
	key_name = "${aws_key_pair.provisioner-key.id}"
	source_dest_check = false

	connection {
    	type     = "ssh"
		user     = "ubuntu"
		private_key = "${file(var.pvt_key)}"
	}

    provisioner "remote-exec" {
    inline = [
       "sudo apt install -y python"
    ]
    }

	# configure openvpn with ansible 
	provisioner "local-exec" {
	    command = "echo -e \"[server]\n${aws_instance.vpn_openvpn.public_dns} ansible_ssh_private_key_file=${var.pvt_key}\" > vpn_openvpn &&  ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu -i vpn_openvpn ../ansible/playbook-openvpn_aws.yml --extra-vars \"openvpn_port=${var.openvpn_port} openvpn_proto=${var.openvpn_proto} openvpn_client_register_dns=false openvpn_port=${var.openvpn_port} openvpn_server_hostname=${aws_instance.vpn_openvpn.public_dns} clients=$HOSTNAME\""
	}

	tags {
	 name = "vpn"
	}
}

resource "aws_security_group" "allow_external" {
	name = "allow_external"
	description = "Allow external in/outbound traffic"

	# allow inbound traffic for openvpn
	ingress {
	  from_port = "${var.openvpn_port}"
	  to_port = "${var.openvpn_port}"
	  protocol = "${var.openvpn_proto}"
	  cidr_blocks = ["0.0.0.0/0"]
	}

	# allow outbound all 
	egress {
	  from_port = 0
	  to_port = 0
	  protocol = "-1"
	   cidr_blocks = ["0.0.0.0/0"]
	}

	# allow ssh
	ingress {
	  from_port = "22"
	  to_port = "22"
	  protocol = "tcp"
	  cidr_blocks = ["0.0.0.0/0"]
	}

	egress {
	  from_port = "22"
	  to_port = "22"
	  protocol = "tcp"
	  cidr_blocks = ["0.0.0.0/0"]
	}
	
	tags {
	 name = "vpn"
	}
}

output "public_ip" {
	value = "${aws_instance.vpn_openvpn.*.public_ip}"
}

output "public_dns" {
	value = "${aws_instance.vpn_openvpn.*.public_dns}"
}
